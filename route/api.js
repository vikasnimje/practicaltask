const express = require('express');
const router = express.Router();
const UserSignUp_controller = require('../controller/UserSignUp.controller');
const ShowUserDetail_controller = require('../controller/ShowUserDetail.controller');
const EditUserDetails_controller = require('../controller/EditUserDetails.controller');
const UserSignIn_controller = require('../controller/UserSignIn.controller');

// UserSign Up  route

router.post('/usercreate', UserSignUp_controller.create_user);

//UserSign In route
router.post('/user/authenticate', UserSignIn_controller.authenticate_user);


// User details route
router.get('/user/:id', ShowUserDetail_controller.show_user );

// Edit user detail route

router.put('/user/:id', EditUserDetails_controller.update_user);

module.exports = router;