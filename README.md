Install Node.js and npm from https://nodejs.org/en/download/.
Install MySQL Community Server from https://dev.mysql.com/downloads/mysql/ and ensure it is started. Installation instructions are available at https://dev.mysql.com/doc/refman/8.0/en/installing.html.
Create database "assignmentDB" to your mysql server. 
Download or clone the project source code from : https://gitlab.com/vikasnimje/practicaltask.git
Install all required npm packages by running npm install from the command line in the project root folder (where the package.json is located).
Start the api by running npm app.js from the command line in the project root folder, you should see the message Server listening on port 8081. 
Open the Postman and follow the routes define in route.js to test the API.