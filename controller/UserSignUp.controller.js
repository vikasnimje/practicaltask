
const db = require('../app/models');
const UserInfo = db.userinfo;
const resourceNames = {
    supportedFormat: ['JPEG', 'JPG', 'PNG', 'PDF', 'BMP', 'TIFF'],
    maxSizeSupported: [4194304]
}
exports.create_user = function (req, res) {
    console.log("Inside user!!");
    // taking file object from request parameter
    const f = req.files.profilePic;
    // check file size should not be more than 4MB
    var filesize = f.size;
    var filename = f.name;
    filename = filename.split('.')
    var filetype = filename[filename.length - 1].toUpperCase();
    if (filesize > resourceNames.maxSizeSupported) {
        res.json({
            "Status": res.statusCode + '',
            "Error_Message": "The file size must be less than 4 MB",

        });
    }
    // check for file type should be image allowing type ('JPEG', 'JPG', 'PNG', 'PDF', 'BMP', 'TIFF')

    else if (!resourceNames.supportedFormat.includes(filetype)) {
        //context.log("requested file format is not supported!");
        res.json({
            "Status": res.statusCode + '',
            "Error_Message": "Requested file format is not supported!! Please use JPEG, PNG, BMP, PDF, and TIFF."

        });

    }
    //console.log("file object :", f);
    else {
        // create user
        const user = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            password: req.body.password,
            profileimage: f.data
        }
        console.log("userobject :", user);
        // save the user in the database
        UserInfo.create(user)
            .then(data => {
                res.send(data);
                console.log("User created!!");
            })
            .catch(err => {
                console.log(err);
                res.status(500).send({
                    message:
                        err.message || "Some error occurred while creating the User."
                });
            });
        
    }
}