const db = require('../app/models');
const UserInfo = db.userinfo;
const config = require('../app/config/db.config');
//var jwt = require('express-jwt');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
exports.authenticate_user = async function (req, res) {
    const email = req.body.email;
    const pwd = req.body.password;
    //UserInfo.findOne()
    UserInfo.scope('withHash').findOne({ where: { email: email } })
        .then(async user => {
            if (!user || !(await bcrypt.compare(pwd, user.hash)))
                res.json({
                    "Status": res.statusCode + '',
                    "Error_Message": "Username or password is incorrect",

                })
            else {
                const token = jwt.sign({ sub: user.id }, config.secret, { expiresIn: '7d' });
                res.json({
                    "Status": res.statusCode + '',
                    "Data": { user, token }

                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred in authentication ."
            });
        });
    /*const user = await UserInfo.scope('withHash').findOne({where : {username : email}});
    if (!user || !(await bcrypt.compare(pwd, user.hash)))
        res.json({
            "Status": res.statusCode + '',
            "Error_Message": "Username or password is incorrect",

        }); 
    else
    {
        const token = jwt.sign({ sub: user.id }, config.secret, { expiresIn: '7d' } );
        res.json({
            "Status": res.statusCode + '',
            "Data": {user, token}

        }); 
    }*/
}