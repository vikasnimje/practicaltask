const db = require('../app/models');
const UserInfo = db.userinfo;
const resourceNames = {
    supportedFormat: ['JPEG', 'JPG', 'PNG', 'PDF', 'BMP', 'TIFF'],
    maxSizeSupported: [4194304]
}
exports.update_user = function (req, res) {
    const id = req.params.id;
    const f = req.files.profilePic;
    // check file size should not be more than 4MB
    var filesize = f.size;
    var filename = f.name;
    filename = filename.split('.')
    var filetype = filename[filename.length - 1].toUpperCase();
    if (filesize > resourceNames.maxSizeSupported) {
        res.json({
            "Status": res.statusCode + '',
            "Error_Message": "The file size must be less than 4 MB",

        });
    }
    // check for file type should be image allowing type ('JPEG', 'JPG', 'PNG', 'PDF', 'BMP', 'TIFF')

    else if (!resourceNames.supportedFormat.includes(filetype)) {
        //context.log("requested file format is not supported!");
        res.json({
            "Status": res.statusCode + '',
            "Error_Message": "Requested file format is not supported!! Please use JPEG, PNG, BMP, PDF, and TIFF."

        });

    }
    else {
        const user = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            password: req.body.password,
            profileimage: f.data
        }
        console.log(user);
        UserInfo.update(user, { where: { userId: id } })
            .then(num => {
                if (num == 1) {
                    res.send({
                        message: "User was updated successfully."
                    });
                } else {
                    res.send({
                        message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
                    });
                }
            })
            .catch(err => {
                console.log("Err :" + err);
                res.status(500).send({
                    message: "Error updating User with id=" + id
                });
            });
    } // end of else
}