const db = require('../app/models');
const UserInfo = db.userinfo;

exports.show_user = function(req, res){
    const id = req.params.id;
    UserInfo.findByPk(id)
        .then(data => {
            res.send(data)
        })
        .catch(err =>{
            res.status(500).send({
                message: "Error retrieving User with id=" + id
              });
        });
}