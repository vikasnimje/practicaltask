const express = require("express");
require('dotenv').config();
const api = require('./route/api');
var app = express();

const bodyParser = require("body-parser");
const cors = require("cors");

const db = require('./app/models');

var fileupload = require('express-fileupload');


db.sequelize.sync();
//If existing tables need to dropped and the database resynced, enter force: true
/*db.sequelize.sync({ force: true }).then(() => {

    console.log("Drop and re-sync db.");
    
});*/

var corsOptions = {
    origin: "http://localhost:8081"
  };
  
app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileupload());

// test the express server
app.get("/", (req, res) => {
    res.send("Welcome to Node MVC!!");
         
    });

//Establish the server connection
//PORT ENVIRONMENT VARIABLE
const port = process.env.PORT;
console.log(port);
app.listen(port, () => console.log(`Listening on port ${port}..`));


//use() on the Express application to add the Router to the middleware handling path,
app.use("/api", api);