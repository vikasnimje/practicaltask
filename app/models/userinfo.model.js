const { sequelize, Sequelize } = require(".");
//In MySQL databases, this model represents userinfo table.
module.exports = (sequelize, Sequelize) => {
    const UserInfo = sequelize.define('UserInfo',{
        userId: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true,
          },
          firstname: {
            type: Sequelize.STRING,
          },
          lastname: {
            type: Sequelize.STRING,
          },
          email: {
            type: Sequelize.STRING,
            unique: true,
          },
          password: {
            type: Sequelize.STRING,
          },
          profileimage: {
            type: Sequelize.BLOB('long'),
          },
         
    });
    return UserInfo;
};